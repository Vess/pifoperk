# ``pifoperk``
**An unhinged library to take care of all your Lua parallelism needs!**

``pifoperk`` is a tiny library that relies on files to implement features that allows you to achieve a limited form of parallelism.

To use it just plop the ``pifoperk/`` into your folder and ``require`` the two files according to your needs, actually using the library is a bit more complicated though. I will soon write documentation on this repository's wiki.

Oh and this has only been tested on Debian