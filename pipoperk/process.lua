-- A table to store all our functions
local process = {}

-- Get the location of the lua interpreter
local lua = arg[-1]

-- A function to create a new process
-- "_" is one of the arguments because I like calling funcions
-- like this "process:new()"
process.new = function(_, func)
	-- A table to hold the process we are creating
	local new_process = {}
	-- Turning the function into bytecode so we can save it in a file
	new_process.bytecode = string.dump(func)
	-- We must then create and open a temporary file
	new_process.file_name = os.tmpname()
	new_process.file = io.open(new_process.file_name, "w+")
	-- Write the bytecode to this new file and flush
	new_process.file:write(new_process.bytecode)
	new_process.file:flush()
	-- NOTE: Maybe close file? There is zero reason to keep it open
	
	-- A function to run the process
	-- We pass the process itself and any arugments we want
	new_process.run = function(self, ...)
		-- we make a little template for running the process
		local run = lua.." "..self.file_name
		-- we then add the arugments to the template
		local arguments = {...}
		for i = 1, #arguments do
			local argument = arguments[i]
			run = run.." "..argument
		end
		-- We then run it
		local output = io.popen(run)
		-- We should return the output
		return output
	end

	-- A function to delete the process
	-- NOTE: I think it'll keep running the background, not sure
	new_process.close = function(self)
		-- we close the file
		self.file:close()
		-- then we delete it
		os.remove(self.file_name)
	end

	-- we return the process
	return new_process
end

return process
