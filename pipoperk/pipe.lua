-- a table to store our functions
local pipe = {}

pipe.new = function(_, name)
	-- a table to store our baby pipe bomb
	local new_pipe = {}

	new_pipe.file_name = name
	-- if we didn't pass a filename for the ppie, we use os.tmpname()
	if not new_pipe.file_name then
		new_pipe.file_name = os.tmpname()
	end

	-- then we make the file
	new_pipe.file = io.open(new_pipe.file_name, "w+")

	-- we write a place holder to the pipe to avoid some issues
	new_pipe.file:write("\" \"")
	new_pipe.file:flush()

	-- a function to write to the pipe
	new_pipe.write = function(self, value)
		-- a limitation is that anything written to the pipe is a string
		-- anyway, get the file
		file = self.file
		-- put the cursor at the beginning
		file:seek("set")
		-- just a lil' check that value exists
		if value then
		-- then we write the value sourrounded by "
			file:write("\"" .. value .. "\"")
		else
			return false
		end
		-- flush
		file:flush()
	end

	-- a function to read from the pipe
	new_pipe.read = function(self)
		-- as usual, we get the file
		file = self.file
		-- we put the cursor at the begining
		file:seek("set")
		-- and then we read all its content
		local file_content = file:read("*a")

		-- we should then look for the value sourrounded by quotation marks
		local b, e = file_content:find("\".*\"")
		if not (b and e) then
			return false
		end
		-- we take the value
		local value = file_content:sub(b, e)
		-- the we remove the quotation marks
		local value = value:sub(2, -2)

		-- and then we just return the value
		return value
	end

	-- a function to close the pipe
	new_pipe.close = function(self)
		-- jusut close it i am tired of coding
		self.file:close()
		-- and then delete it
		os.remove(self.file_name)
	end

	-- NOTE: maybe add another function to remove variables

	-- then we return the pipe
	return new_pipe
end

pipe.connect = function(self, name)
	local connected_pipe = self:new(name)
	return connected_pipe
end

return pipe
